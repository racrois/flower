
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import entities.Flower;
import entities.SaleItem;
import repository.FlowerRepository;
import repository.SaleItemRepository;
import repository.SaleRepository;

public class Main {

	public static void main(String[] args) {
		FlowerRepository flowerRepo = new FlowerRepository();
		SaleRepository saleRepo = new SaleRepository();
		SaleItemRepository saleItemRepo = new SaleItemRepository();

		// --------------------MENÜ------------------

		int menu = 0;
		Scanner scanner = new Scanner(System.in);

		do {
			System.out.println("\n----------------------MENÜ----------------------");

			System.out.println("\nKérem válasszon az alabbi menüpontok közül:");
			System.out.println("\t 1. - Virág felvitele");
			System.out.println("\t 2. - Lekérdezések");
			System.out.println("\t 3. - Eladás felvitele");
			System.out.println("\t 4. - Kilépés");

			try {
				menu = scanner.nextInt();

				scanner.nextLine();
			} catch (InputMismatchException ex) {
				System.err.println("Csak számokat adhatsz meg! Probáld újra!");
				scanner.nextLine();
				continue;
			}

			switch (menu) {

			case 1:
				System.out.println("\nKérem válasszon az alabbi almenüpontok közül:");
				System.out.println("\t 1. - Meglévõ virág mennyiségének növelése");
				System.out.println("\t 2. - Új virág felvitele");

				int almenu = 0;

				try {
					almenu = scanner.nextInt();
					scanner.nextLine();

				} catch (InputMismatchException ex) {
					System.err.println("Csak számokat adhatsz meg!");
					scanner.nextLine();
					continue;
				}

				switch (almenu) {
				case 1:

					System.out.println("Kérlek add meg a virág színét:");
					String color = scanner.next();
					System.out.println(color);

					System.out.println("Kérlek add meg a virág típusát:");
					String type = scanner.next();
					System.out.println(type);

					if (!flowerRepo.checkFlowerExist(color, type)) {

						System.out.println("Figyelmeztetés: Nincs ilyen virág! Kérlek írd be újra!");
						break;

					} else {
						Flower flower = flowerRepo.getFlower(color, type);

						Integer currentDb = flower.getDb();

						System.out.format("Az eddigi darabszám: %d %n", currentDb);
						System.out.println("Kérlek add meg a beszállító által hozott plusz darabszámot:");
						Integer increment = scanner.nextInt();

						flowerRepo.updateFlower(flower, currentDb + increment);

						System.out.format("Az új darabszám: %d %n", currentDb + increment);

						System.out.println("\nKilépett!");
						System.exit(0);
					}

				case 2:

					Flower newFlower = new Flower();

					System.out.println("Kérlek add meg a virág színét:");
					newFlower.setColor(scanner.next());
					System.out.println(newFlower.getColor());

					System.out.println("Kérlek add meg a virág típusát:");
					newFlower.setType(scanner.next());
					System.out.println(newFlower.getType());

					System.out.println("Kérlek add meg a virág egységárát:");
					newFlower.setUnitPrice(scanner.nextInt());
					System.out.println(newFlower.getUnitPrice());

					System.out.println("Kérlek add meg a virág darabszámát:");
					newFlower.setDb(scanner.nextInt());
					System.out.println(newFlower.getDb());

					if (!flowerRepo.checkFlowerExist(newFlower.getColor(), newFlower.getType())) {

						flowerRepo.insertFlower(newFlower);
					} else {
						System.out.println("Ez a virág már létezik!");
					}
					break;

				default:
					break;
				}
				// Főmenü, lekérdezések
			case 2:
				System.out.println("\nKérem válasszon az alabbi almenüpontok közül:");
				System.out.println("\t 1. - Virágok listázása");
				System.out.println("\t 2. - Eladások listázása");
				System.out.println("\t 3. - Virágra és eladás dátumára szűrés");

				int almenu2 = 0;

				try {
					almenu2 = scanner.nextInt();
					scanner.nextLine();

				} catch (InputMismatchException ex) {
					System.err.println("Csak számokat adhatsz meg!");
					scanner.nextLine();
					continue;
				}

				switch (almenu2) {
				case 1:

					System.out.println("\nAz összes virág listázása:");
					flowerRepo.findAll().forEach(System.out::println);

					System.exit(0);

				case 2:

					System.out.println("\nAz összes eladás listázása:");
					saleRepo.findAll().forEach(System.out::println);
					System.out.println("Válassz egy eladás sorszámot a részletekhez:");
					Integer saleId = scanner.nextInt();
					Integer sumValue = 0;
					List<Integer> prices = new ArrayList<>();
					List<SaleItem> items = saleItemRepo.getSaleItems(saleId);

					if (items.isEmpty()) {
						System.out.println("Nincs ilyen eladás!");
						System.exit(0);
					}

					items.forEach((item) -> {
						// System.out.println(item.getFlowerId());
						Flower flower = flowerRepo.getFlowerById(item.getFlowerId());
						String price = flower.getUnitPrice().toString();

						System.out.format("%-10s %-10s %d db %d Ft/db %d Ft %n", flower.getColor(), flower.getType(),
								item.getDb(), flower.getUnitPrice(), item.getDb() * flower.getUnitPrice());
						prices.add(item.getDb() * flower.getUnitPrice());
					});
					sumValue = prices.stream().mapToInt(Integer::intValue).sum();

					System.out.println(
							"--------------------------------------------------------------------------------------");
					System.out.format("Összesen:                            %d Ft %n", sumValue);
					System.out.println("\nKilépett!");
					System.exit(0);

				case 3:

					System.out.println("Kérlek add meg a virág színét:");
					String color = scanner.next();
					System.out.println(color);

					System.out.println("Kérlek add meg a virág típusát:");
					String type = scanner.next();
					System.out.println(type);

					System.out.println("Kérlek add meg a szűrés kezdő dátumát '2015-03-13' formátumban: ");
					String date = scanner.next();
					LocalDate localDate = LocalDate.parse(date);

					System.out.println("Kérlek add meg a szűrés vég dátumát '2015-03-13' formátumban: ");
					String date2 = scanner.next();
					LocalDate localDate2 = LocalDate.parse(date2);

					saleRepo.findByFlowerAndDate(color, type, localDate, localDate2).forEach(System.out::println);

					System.out.println("Válassz egy eladás sorszámot a részletekhez:");
					Integer saleId2 = scanner.nextInt();
					Integer sumValue2 = 0;
					List<Integer> prices2 = new ArrayList<>();
					List<SaleItem> items2 = saleItemRepo.getSaleItems(saleId2);

					if (items2.isEmpty()) {
						System.out.println("Nincs ilyen eladás!");
						System.exit(0);
					}

					items2.forEach((item) -> {
						// System.out.println(item.getFlowerId());
						Flower flower = flowerRepo.getFlowerById(item.getFlowerId());

						System.out.format("%-10s %-10s %d db %d Ft/db %d Ft %n", flower.getColor(), flower.getType(),
								item.getDb(), flower.getUnitPrice(), item.getDb() * flower.getUnitPrice());
						prices2.add(item.getDb() * flower.getUnitPrice());
					});
					sumValue2 = prices2.stream().mapToInt(Integer::intValue).sum();

					System.out.println(
							"--------------------------------------------------------------------------------------");
					System.out.format("Összesen:                            %d Ft %n", sumValue2);

					System.out.println("Kilépett");
					System.exit(0);
				default:
					break;
				}

			case 3:
				System.out.println("Eladás felvitele");
				System.out.println("Kérlek add meg az eladás dátumát '2015-03-13' formátumban: ");
				String date = scanner.next();
				LocalDate localDate = LocalDate.parse(date);
				Integer saleId = saleRepo.insertSale(localDate);
				boolean hasNewItem = true;

				while (hasNewItem) {

					System.out.println("Kérlek add meg a virág színét:");
					String color = scanner.next();
					System.out.println(color);

					System.out.println("Kérlek add meg a virág típusát:");
					String type = scanner.next();
					System.out.println(type);

					Flower flower = flowerRepo.getFlower(color, type);

					System.out.println("Kérlek add meg a virág darabszámát:");
					Integer db = scanner.nextInt();
					System.out.println(db);

					if (db > flower.getDb()) {
						System.out.println("Nincs ennyi darab a készleten! ");
					} else {
						saleItemRepo.insertSaleItem(saleId, flower.getFlowerId(), db);
						flowerRepo.updateFlower(flower, flower.getDb() - db);
					}

					System.out.println("Szeretnél még újabb tételt (igen=1 vagy nem=2)?:");

					try {
						Integer answer = scanner.nextInt();
						scanner.nextLine();

						if (answer != 1) {
							hasNewItem = false;
						}
						System.out.println(hasNewItem);
					} catch (InputMismatchException ex) {
						System.err.println("Csak számokat adhatsz meg!");
						hasNewItem = false;
					}
				}
				System.out.format("Eladás sorszáma: %d %n", saleId);

				Integer sumValue = 0;
				List<Integer> prices = new ArrayList<>();
				List<SaleItem> items = saleItemRepo.getSaleItems(saleId);
				items.forEach((item) -> {
					// System.out.println(item.getFlowerId());
					Flower flower = flowerRepo.getFlowerById(item.getFlowerId());
					String price = flower.getUnitPrice().toString();

					System.out.format("%-10s %-10s %d db %d Ft/db %d Ft %n", flower.getColor(), flower.getType(),
							item.getDb(), flower.getUnitPrice(), item.getDb() * flower.getUnitPrice());
					prices.add(item.getDb() * flower.getUnitPrice());
				});
				sumValue = prices.stream().mapToInt(Integer::intValue).sum();

				System.out.println(
						"--------------------------------------------------------------------------------------");
				System.out.format("Összesen:                            %d Ft %n", sumValue);

				break;

			case 4:
				System.out.println("\nKilépett!");
				break;

			default:
				System.out.println("Ez a menüpont nem létezik! Kérlek a felkínált menüpontok sorszámaiból válassz!");
				break;
			}
		} while (menu != 4);

		scanner.close();

	}

}
