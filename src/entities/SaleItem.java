package entities;

public class SaleItem {

	private Integer flowerId;
	private Integer db;
	private Integer saleId;
	private Integer saleItemId;

	public SaleItem(Integer flowerId, Integer db, Integer saleId, Integer saleItemId) {
		super();
		this.flowerId = flowerId;
		this.db = db;
		this.saleId = saleId;
		this.saleItemId = saleItemId;
	}

	public Integer getFlowerId() {
		return flowerId;
	}

	public void setFlowerId(Integer flowerId) {
		this.flowerId = flowerId;
	}

	public Integer getDb() {
		return db;
	}

	public void setDb(Integer db) {
		this.db = db;
	}

	public Integer getSaleId() {
		return saleId;
	}

	public void setSaleId(Integer saleId) {
		this.saleId = saleId;
	}

	public Integer getSaleItemId() {
		return saleItemId;
	}

	public void setSaleItemId(Integer saleItemId) {
		this.saleItemId = saleItemId;
	}

	public SaleItem() {
		super();
	}

	@Override
	public String toString() {
		return "SaleItem [flowerId=" + flowerId + ", db=" + db + ", saleId=" + saleId + ", saleItemId=" + saleItemId
				+ "]";
	}

}
