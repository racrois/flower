package entities;

public class Flower {

	private Integer flowerId;
	private String color;
	private String type;
	private Integer unitPrice;
	private Integer db;

	public Flower(Integer flowerId, String color, String type, Integer unitPrice, Integer db) {
		super();
		this.flowerId = flowerId;
		this.color = color;
		this.type = type;
		this.unitPrice = unitPrice;
		this.db = db;
	}

	public Flower(String color, String type, Integer unitPrice, Integer db) {
		super();
		this.color = color;
		this.type = type;
		this.unitPrice = unitPrice;
		this.db = db;
	}

	public Flower(String color, String type, Integer unitPrice) {
		super();
		this.color = color;
		this.type = type;
		this.unitPrice = unitPrice;
	}

	public Flower() {
		super();
	}

	public Integer getFlowerId() {
		return flowerId;
	}

	public void setFlowerId(Integer flowerId) {
		this.flowerId = flowerId;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getDb() {
		return db;
	}

	public void setDb(Integer db) {
		this.db = db;
	}

	@Override
	public String toString() {
		return "Flower [flowerId=" + flowerId + ", color=" + color + ", type=" + type + ", unitPrice=" + unitPrice
				+ ", db=" + db + "]";
	}

}