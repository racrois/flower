package entities;

import java.time.LocalDate;

public class Sale {

	private Integer saleId;
	private LocalDate saleDate;

	public Sale(Integer saleId, LocalDate saleDate) {
		super();
		this.saleId = saleId;
		this.saleDate = saleDate;
	}

	public Sale() {
	}

	public Integer getSaleId() {
		return saleId;
	}

	public void setSaleId(Integer saleId) {
		this.saleId = saleId;
	}

	public LocalDate getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(LocalDate saleDate) {
		this.saleDate = saleDate;
	}

	@Override
	public String toString() {
		return "Sale [saleId=" + saleId + ", saleDate=" + saleDate + "]";
	}

}
