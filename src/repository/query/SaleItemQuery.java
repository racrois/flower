package repository.query;

public enum SaleItemQuery {

	FIND_BY_SALE("SELECT sale_item.* FROM sale_item where sale_id = %s");

	private String query;

	private SaleItemQuery(String query) {
		this.query = query;
	}

	public String toString() {
		return query;
	}

}
