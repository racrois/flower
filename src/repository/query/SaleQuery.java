package repository.query;

public enum SaleQuery {

	FIND_ALL("SELECT * FROM sale JOIN sale_item ON sale.sale_id = sale_item.sale_id"),
	FIND_BY_FLOWER_AND_DATE(
			"SELECT * FROM sale JOIN sale_item ON sale.sale_id = sale_item.sale_id JOIN"
			+ " flower ON sale_item.flower_id=flower.flower_id WHERE flower.color = '%s' AND flower.type = '%s' "
			+ "AND sale.sale_date >= '%s' AND sale.sale_date <= '%s'");
	private String query;

	private SaleQuery(String query) {
		this.query = query;
	}

	public String toString() {
		return query;
	}

}
