package repository.query;

public enum FlowerQuery {

	FIND_ALL("SELECT * FROM flower");

	private String query;

	private FlowerQuery(String query) {
		this.query = query;
	}

	public String toString() {
		return query;
	}

}
