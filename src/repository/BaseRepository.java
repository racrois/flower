package repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class BaseRepository {

	private static final String DB_URL = "jdbc:mysql://localhost:3306/flower_shop";
	private static final String DB_USER = "root";
	private static final String DB_PASSWORD = "";

	protected Connection conn;

	public BaseRepository() {
		initDatabase();
	}

	private void initDatabase() {
		try {
			conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			System.out.println("Az adatb�zis kapcsolat l�trej�tt!");
		} catch (SQLException e) {
			System.err.println("Hiba az adatb�zis kapcsolat l�trehoz�sa k�zben! " + e);
		}
	}

	protected <T> List<T> queryList(String sqlQuery, Function<ResultSet, T> mapper) throws SQLException {

		List<T> result = new ArrayList<>();

		Statement stmt = conn.createStatement();
		ResultSet resultSet = stmt.executeQuery(sqlQuery);

		while (resultSet.next()) {
			result.add(mapper.apply(resultSet));
		}

		return result;

	}

	public void closeConnection() {
		try {
			if (conn != null) {
				conn.close();
			} else {
				System.out.println("Nem is volt nyitva!");
			}
			System.out.println("Ha volt kapcsolat, az le lett z�rva");
		} catch (SQLException e) {
			System.err.println("Hiba a kapcsolat lez�r�s�ban! " + e);
		}
	}

}
