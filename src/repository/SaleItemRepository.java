package repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entities.SaleItem;
import repository.mapper.SaleItemMapper;
import repository.query.SaleItemQuery;

public class SaleItemRepository extends BaseRepository {

	private SaleItemMapper mapper = new SaleItemMapper();

	public SaleItemRepository() {
		super();
	}

	public void insertSaleItem(Integer sale_id, Integer flower_id, Integer db) {

		String query = "insert into sale_item(sale_id, flower_id, db) " + "values(?,?,?)";
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, sale_id);
			pstmt.setInt(2, flower_id);
			pstmt.setInt(3, db);
			pstmt.execute();

		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [insertSaleItem] " + e);
		}
	}

	public List<SaleItem> getSaleItems(Integer saleId) {
		List<SaleItem> result = new ArrayList<>();

		try {

			result = queryList(String.format(SaleItemQuery.FIND_BY_SALE.toString(), saleId), mapper);

		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [findAll] " + e);
		}
		return result;

	}

}