package repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.function.Function;

import entities.Sale;

public class SaleMapper implements Function<ResultSet, Sale> {

	@Override
	public Sale apply(ResultSet resultSet) {

		Sale sale = new Sale();
		try {
			sale.setSaleId(resultSet.getInt("sale_id"));
			LocalDate localDate = LocalDate.parse(resultSet.getDate("sale_date").toString());
			sale.setSaleDate(localDate);

			return sale;

		} catch (SQLException e) {
			System.out.println("Hiba Sale l�trehoz�s k�zben");
			System.out.println(e);

		}
		return null;
	}

}
