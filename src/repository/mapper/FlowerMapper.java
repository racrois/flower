package repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

import entities.Flower;

public class FlowerMapper implements Function<ResultSet, Flower> {

	@Override
	public Flower apply(ResultSet resultSet) {

		Flower flower = new Flower();
		try {
			flower.setFlowerId(resultSet.getInt("flower_id"));
			flower.setColor(resultSet.getString("color"));
			flower.setType(resultSet.getString("type"));
			flower.setUnitPrice(resultSet.getInt("unitPrice"));
			flower.setDb(resultSet.getInt("db"));

			return flower;

		} catch (SQLException e) {
			System.out.println("Hiba Flower l�trehoz�s k�zben");

		}
		return null;
	}

}
