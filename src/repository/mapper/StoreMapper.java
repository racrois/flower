package repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

import entities.SaleItem;

public class StoreMapper implements Function<ResultSet, SaleItem>{

	@Override
	public SaleItem apply(ResultSet resultSet) {
		
		SaleItem ret = new SaleItem();
		try {
		ret.setFlowerId(resultSet.getInt("flower_id"));
		ret.setDb(resultSet.getInt("db"));
		
		return ret;
		
		}catch (SQLException e) {
			System.out.println("Hiba Store l�trehoz�s k�zben");
			
		}
		return null;
	}

}
