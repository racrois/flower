package repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Flower;
import repository.mapper.FlowerMapper;
import repository.query.FlowerQuery;

public class FlowerRepository extends BaseRepository {

	private FlowerMapper mapper = new FlowerMapper();

	public FlowerRepository() {
		super();
	}

	public List<Flower> findAll() {
		List<Flower> result = new ArrayList<>();

		try {

			result = queryList(FlowerQuery.FIND_ALL.toString(), mapper);

		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [findAll] " + e);
		}
		return result;
	}

	public Integer insertFlower(Flower flower) {
		Integer id = null;
		String query = "insert into " + "flower(color, type, unitPrice, db) " + "values(?,?,?,?)";
		try {
			PreparedStatement pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, flower.getColor());
			pstmt.setString(2, flower.getType());
			pstmt.setInt(3, flower.getUnitPrice());
			pstmt.setInt(4, flower.getDb());
			pstmt.execute();
			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			id = rs.getInt(1);
		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [insertFlower] " + e);
		}
		return id;
	}

	public boolean checkFlowerExist(String color, String type) {

		String query = String.format("select * from flower where color = '%s' AND type = '%s'", color, type);

		try {
			List<Flower> queryList = null;
			queryList = queryList(query, mapper);
			return !queryList.isEmpty();
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}

	public Flower getFlower(String color, String type) {

		Flower result = null;
		String query = String.format("select * from flower where color = '%s' AND type = '%s'", color, type);

		try {
			Statement stmt = conn.createStatement();
			ResultSet resultSet = stmt.executeQuery(query);
			while (resultSet.next()) {
				result = new Flower();
				result.setFlowerId(resultSet.getInt("flower_id"));
				result.setColor(resultSet.getString("color"));
				result.setType(resultSet.getString("type"));
				result.setUnitPrice(resultSet.getInt("unitPrice"));
				result.setDb(resultSet.getInt("db"));
				return result;
			}
		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [getFlower] " + e);
		}
		return result;
	}

	public Flower getFlowerById(Integer id) {

		Flower result = null;
		String query = String.format("select * from flower where flower_id = %s", id);

		try {
			Statement stmt = conn.createStatement();
			ResultSet resultSet = stmt.executeQuery(query);
			while (resultSet.next()) {
				result = new Flower();
				result.setFlowerId(resultSet.getInt("flower_id"));
				result.setColor(resultSet.getString("color"));
				result.setType(resultSet.getString("type"));
				result.setUnitPrice(resultSet.getInt("unitPrice"));
				result.setDb(resultSet.getInt("db"));
				return result;
			}
		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [getFlower] " + e);
		}
		return result;
	}

	public void updateFlower(Flower flower, Integer db) {

		String query = "update flower SET db = ? WHERE flower_id=?";

		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, db);
			pstmt.setInt(2, flower.getFlowerId());
			pstmt.execute();

		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [updateFlower] " + e);
		}

	}

}
