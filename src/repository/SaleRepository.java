package repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import entities.Sale;
import repository.mapper.SaleMapper;
import repository.query.SaleQuery;

public class SaleRepository extends BaseRepository {

	private SaleMapper mapper = new SaleMapper();

	public Integer insertSale(LocalDate saleDate) {
		Integer id = null;
		String query = "insert into sale(sale_date) values(?)";
		try {
			PreparedStatement pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			pstmt.setDate(1, java.sql.Date.valueOf(saleDate));
			pstmt.execute();
			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			id = rs.getInt(1);
		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [insertSale] " + e);
		}
		return id;
	}

	public List<Sale> findAll() {

		List<Sale> result = new ArrayList<>();

		try {

			result = queryList(SaleQuery.FIND_ALL.toString(), mapper);

		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [findAll] " + e);
		}
		return result;
	}

	public List<Sale> findByFlowerAndDate(String color, String type, LocalDate startDate, LocalDate endDate) {

		List<Sale> result = new ArrayList<>();

		try {

			result = queryList(
					String.format(SaleQuery.FIND_BY_FLOWER_AND_DATE.toString(), color, type, startDate, endDate),
					mapper);

		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [findAll] " + e);
		}
		return result;
	}

}
