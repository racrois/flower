package repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import entities.Flower;
import entities.SaleItem;
import repository.mapper.StoreMapper;

//minden tĂˇblĂˇnak amivel java oldalon dolgozok, kĂĽlĂ¶n repository
public class StoreRepository extends BaseRepository {

	private StoreMapper mapper = new StoreMapper();

	public StoreRepository() {
		super(); // a szülőosztály konstruktorára utal
		// initDatabase(); ősosztályban
	}

	public Integer insertStore(SaleItem store) {

		String query = "insert into " + "store(flower_id, db) " + "values(?,?)";
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, store.getFlowerId());
			pstmt.setInt(2, store.getDb());
			pstmt.execute();

		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [insertStore] " + e);
		}
		return store.getFlowerId();
	}
	
	public SaleItem select(Integer flowerId) {
		
		SaleItem ret = null;
		String query = "select * from store WHERE flower_id = %d";
		
		try {
			List<SaleItem> queryList = queryList(String.format(query,flowerId), mapper);
			ret=queryList.get(0);

		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [updateStore] " + e);
		}
		return ret;
		
	}
	
	public void updateStore(SaleItem store) {

		String query = "update store SET db = ? WHERE flower_id=?";
		
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, store.getDb());
			pstmt.setInt(2, store.getFlowerId());
			pstmt.execute();

		} catch (SQLException e) {
			System.err.println("Hiba a lekérdezésben [updateStore] " + e);
		}

	}

	
	/*
	 * public List<Flower> findByColor(String color){ List<Flower> result = new
	 * ArrayList<>();
	 * 
	 * try { //Statement stmt = conn.createStatement(); //ResultSet resultSet =
	 * stmt.executeQuery(query); //ResultSet resultSet = query(sql); result =
	 * queryList(String.format(FlowerQuery.FIND_BY_COLOR.toString(), color),
	 * mapper); //result = queryList(FlowerQuery. , mapper);
	 * 
	 * //FlowerMapper mapper = new FlowerMapper();
	 * 
	 * //while (resultSet.next()) { /*Flower flower = new Flower();
	 * flower.setFlowerId(resultSet.getInt("flower_id"));
	 * flower.setColor(resultSet.getString("color"));
	 * flower.setType(resultSet.getString("type"));
	 * flower.setUnitPrice(resultSet.getInt("unitPrice")); //alt shift L
	 * //result.add(flower); result.add(mapper.apply(resultSet)); } } catch
	 * (SQLException e) { System.err.println("Hiba a lekďż˝rdezďż˝sben [findAll] " +
	 * e); } return result; }
	 */

}

/*
 * package repository;
 * 
 * import java.sql.Connection; import java.sql.DriverManager; import
 * java.sql.PreparedStatement; import java.sql.ResultSet; import
 * java.sql.SQLException; import java.sql.Statement; import java.util.ArrayList;
 * import java.util.List;
 * 
 * import entities.Person;
 * 
 * public class Repository {
 * 
 * private static final String DB_URL = "jdbc:mysql://localhost:3306/pets";
 * private static final String DB_USER = "root"; private static final String
 * DB_PASSWORD = "";
 * 
 * private Connection conn;
 * 
 * private void initDatabase() { try { conn =
 * DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
 * System.out.println("A kapcsolat lďż˝trejďż˝tt!"); } catch (SQLException e) {
 * System.err.println("Hiba az adatbďż˝zis kapcsolat lďż˝trehozďż˝sa kďż˝zben! "
 * + e); } }
 * 
 * public Repository() { initDatabase(); }
 * 
 * public void closeConnection() { try { if (conn != null) { conn.close(); }
 * else { System.out.println("Nem is volt nyitva!"); }
 * System.out.println("Ha volt kapcsolat, az le lett zďż˝rva"); } catch
 * (SQLException e) { System.err.println("Hiba a kapcsolat lezďż˝rďż˝sďż˝ban! "
 * + e); } }
 * 
 * public List<Person> findAll() { List<Person> result = new ArrayList<>();
 * String query = "select * from person"; try { Statement stmt =
 * conn.createStatement(); ResultSet resultSet = stmt.executeQuery(query); while
 * (resultSet.next()) { Person person = new Person();
 * person.setPersonId(resultSet.getInt("person_id"));
 * person.setName(resultSet.getString("name"));
 * person.setMail(resultSet.getString("mail"));
 * person.setPhone(resultSet.getString("phone"));
 * person.setSecondaryPhone(resultSet.getString("secondary_phone"));
 * result.add(person); } } catch (SQLException e) {
 * System.err.println("Hiba a lekďż˝rdezďż˝sben [findAll] " + e); } return
 * result; }
 * 
 * public Person findByName(String name) { Person result = null; String query =
 * "select * from person where name like '" + name + "'"; try { Statement stmt =
 * conn.createStatement(); ResultSet resultSet = stmt.executeQuery(query); while
 * (resultSet.next()) { result = new Person();
 * result.setPersonId(resultSet.getInt("person_id"));
 * result.setName(resultSet.getString("name"));
 * result.setMail(resultSet.getString("mail"));
 * result.setPhone(resultSet.getString("phone"));
 * result.setSecondaryPhone(resultSet.getString("secondary_phone")); return
 * result; } } catch (SQLException e) {
 * System.err.println("Hiba a lekďż˝rdezďż˝sben [findByName] " + e); } return
 * result; }
 * 
 * public void numberOfPeople() { String query =
 * "select count(*) as 'Total' from person"; try { Statement stmt =
 * conn.createStatement(); ResultSet rs = stmt.executeQuery(query);
 * while(rs.next()) { System.out.println(rs.getInt("Total")); } } catch
 * (SQLException e) { // TODO Auto-generated catch block
 * System.err.println("Hiba a lekďż˝rdezďż˝sben [numberOfPeople] " + e); } }
 * 
 * public Integer insertPerson(Person person) { Integer id = null; String query
 * = "insert into " + "person(name, mail, phone, secondary_phone) " +
 * "values(?,?,?,?)"; try { PreparedStatement pstmt =
 * conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
 * pstmt.setString(1, person.getName()); pstmt.setString(2, person.getMail());
 * pstmt.setString(3, person.getPhone()); pstmt.setString(4,
 * person.getSecondaryPhone()); pstmt.execute(); ResultSet rs =
 * pstmt.getGeneratedKeys(); rs.next(); id = rs.getInt(1); } catch (SQLException
 * e) { System.err.println("Hiba a lekďż˝rdezďż˝sben [insertPerson] " + e); }
 * return id; }
 * 
 * public int deleteRecord(String table, String column, int id) { String query =
 * "delete from " + table + " where " + column + " = ?"; PreparedStatement
 * pstmt; try { pstmt = conn.prepareStatement(query); pstmt.setInt(1, id);
 * return pstmt.executeUpdate(); } catch (SQLException e) {
 * System.err.println("Hiba a lekďż˝rdezďż˝sben [deleteRecord] " + e); } return
 * 0; } }
 */
