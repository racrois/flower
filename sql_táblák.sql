



CREATE TABLE `flower` (
  `flower_id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(20) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `unitPrice` int(20) DEFAULT NULL,
  `db` int(20) DEFAULT NULL,
  PRIMARY KEY (`flower_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci	


CREATE TABLE `sale` (
  `sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_date` date DEFAULT NULL,
  PRIMARY KEY (`sale_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci	



CREATE TABLE `sale_item` (
  `sale_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(8) DEFAULT NULL,
  `flower_id` int(3) DEFAULT NULL,
  `db` int(5) DEFAULT NULL,
  PRIMARY KEY (`sale_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci	
